
import { Person } from './person.models';

describe('Test for PersonModel', () => {

  // Code 
  describe('Test for person.getFullName', () => {

    it('should return an string with name + lastname', () => {
      const person = new Person('Sergio', 'Villagomez', 24);
      expect(person.getFullName()).toEqual('Sergio Villagomez');
    });

  });

  describe('Test for person.getAgeInYears', () => {

    it('should return 34 years', () => {
      const person = new Person('Sergio', 'Villagomez', 24);
      const age = person.getAgeInYears(10);
      expect(age).toEqual(34);
    });

    it('should return 40 years', () => {
      const person = new Person('Sergio', 'Villagomez', 20);
      const age = person.getAgeInYears(20);
      expect(age).toEqual(40);
    });

    it('should return 50 years with negative number', () => {
      const person = new Person('Sergio', 'Villagomez', 50);
      const age = person.getAgeInYears(-10);
      expect(age).toEqual(50);
    });

  });
  
});